<?php
/**
 * @file
 * Context condition plugin for smart_ip.
 */

/**
 * Exposes smart_ip information as context condition.
 */
class context_smart_ip_condition extends context_condition {
  function condition_values() {
    return array();
  }

  function condition_form($context) {
    $values = $this->fetch_from_context($context, 'values');
    $form = parent::condition_form($context);
    $form['#type'] = 'fieldset';
    $form['#tree'] = TRUE;
    $form['country'] = array(
      '#title' => t('Country codes'),
      '#type' => 'textfield',
      '#descripion' => t('Country codes separated by spaces.'),
      '#default_value' => $values ? $values['country'] : '',
    );
    return $form;
  }

  function condition_form_submit($values) {
    return $values;
  }

  function execute() {
    foreach ($this->get_contexts() as $context) {
      // see if we have any condition options to compare
      $values = $this->fetch_from_context($context, 'values');
      if (!empty($values)) {
        // load smart ip data if found
        $session_data = array();
        if (module_exists('session_cache')) {
          $session_data = session_cache_get('smart_ip');
        }
        else {
          $session_data = isset($_SESSION) && isset($_SESSION['smart_ip']) ? $_SESSION['smart_ip'] : NULL;
        }
        // if smart ip is not initialized in this session yet then initialize it
        if (empty($session_data) || empty($session_data['location'])) {
          $roles_to_geo_locate = variable_get('smart_ip_roles_to_geolocate', array(DRUPAL_AUTHENTICATED_RID));
          if (in_array(DRUPAL_ANONYMOUS_RID, $roles_to_geo_locate)) {
            global $user;
            drupal_load('module', 'smart_ip');
            $location = smart_ip_get_current_visitor_location_data();
            smart_ip_set_location_data($user, $location);
            $session_data = module_exists('session_cache') ? session_cache_get('smart_ip') : (isset($_SESSION) && isset($_SESSION['smart_ip']) ? $_SESSION['smart_ip'] : NULL);
          }
          if (empty($session_data['location'])) {
            // it will fail with error if method of smart ip is ip2info
            // it seems this method requires to be loaded in a later stage
            watchdog('context_smart_ip', 'Region detection using ip (smart_ip) failed !');
            return FALSE;
          }
        }
        $countries = explode(' ', $values['country']);
        // compare detected country to condition value entered in form
        if (isset($session_data['location']['country_code']) && in_array($session_data['location']['country_code'], $countries)) {
          $this->condition_met($context);
        }
      }
    }
  }
}
